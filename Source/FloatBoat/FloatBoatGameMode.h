// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "FloatBoatGameMode.generated.h"

UCLASS(minimalapi)
class AFloatBoatGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFloatBoatGameMode();
};



