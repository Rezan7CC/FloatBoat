// Fill out your copyright notice in the Description page of Project Settings.

#include "FloatBoat.h"
#include "FloorController.h"


// Sets default values
AFloorController::AFloorController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFloorController::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFloorController::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

